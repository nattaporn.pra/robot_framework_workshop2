*** Settings ***
Library  BuiltIn
Library  SeleniumLibrary
Library  Collections

*** Variables ***
${url}=  https://ptt-devpool-robotframework.herokuapp.com
${username}=  admin
${password}=  1234
&{data1}=  name=Nattaporn  last=Pradit  Email=znth.p@gmail.com
&{data2}=  name=Praephat  last=Eiei  Email=zparepare.phh@gmail.com
&{data3}=  name=Piyarat  last=Boonboon  Email=zpiyarat.pipi@gmail.com
&{data4}=  name=Ntp  last=pd  Email=newnewnew.p@gmail.com
@{ListData}=  data1  data2  data3  data4

*** Test Cases ***
1. เปิด Browser
    Open Browser  about:blank  browser=Chrome
    Maximize Browser Window
    Set Selenium Speed  0.5
    Go to  ${url}

2. ระบุ Username และ password
    Input text  id=txt_username  ${username}
    Input text  id=txt_password  ${password}

3. คลิกปุ่ม Sign in
    Click Button  id=btn_login


4. เพิ่มข้อมูล ด้วยปุ่ม New Data
    FOR  ${index}  IN  @{ListData}
        LoopInputData  &{${index}}
    END

5. Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png

   
*** Keywords ***
LoopInputData
    Click Button  id=btn_new
    [Arguments]  &{paraKey}
    FOR  ${key1}  ${key2}  IN   &{paraKey}
            Run Keyword If  '${key1}' == 'name'  Input text  id=txt_new_firstname  ${paraKey["${key1}"]} 
            Run Keyword if  '${key1}' == 'last'  Input text  id=txt_new_lastname  ${paraKey["${key1}"]}
            Run Keyword if  '${key1}' == 'Email'  Input text  id=txt_new_email  ${paraKey["${key1}"]} 
    END
    Click Button  id=btn_new_save
